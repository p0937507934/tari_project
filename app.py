from flask import Flask, jsonify, render_template, redirect, url_for, request
from flask_pymongo import PyMongo

app = Flask("TARI_Project")
app.config["MONGO_URI"] = "mongodb://localhost:27017/TARI_DBS"
mongo = PyMongo(app)


@app.route('/rest/ping', methods=["GET"])
def ping():
    result = {
        "code": 200,
        "status": "OK",
        "message": "ping successful",
        "description": "Welcome to restful api serever."
    }
    return jsonify(result)


@app.route("/")
def index():
    return 'hello world'


@app.route("/find/all")
def find_hyperspectral():

    results = mongo.db.hyperspectral.find()
    if results:
        return render_template('index.html', results=results)


@app.route("/insert", methods=["POST"])
def insert_hyperspectral():
    if request.method == "POST":
        Datatype = request.values["Datatype"]
        Camera_type = request.values["Camera_type"]
        File_name = request.values["File_name"]

        results = mongo.db.hyperspectral.insert_one({
            "Datatype": Datatype,
            "Camera_type": Camera_type,
            "File_name": File_name

        })
        return "insert data successful"
    return "fail to insert"


@app.route("/find/<string:id>/<id2>", methods=["GET"])
def find_hyperspectral_filter(id, id2):
    print(id)

    if id == "Datatype" or id == "datatype":
        results = mongo.db.hyperspectral.find({
            "Datatype": id2
        })
        print("this is col1")
        return render_template('index.html', results=results)

    elif id == "Camera_type" or id == "camera_type":

        results = mongo.db.hyperspectral.find({
            "Camera_type": id2
        })

        print("this is col2")
        return render_template('index.html', results=results)

    else:
        return "something wrong"

    return 'nothing'


@app.route("/update/<id>/<id2>/<id3>")
def update_hyperspectral():
    results = mongo.db.hyperspectral


@app.route("/delete/<id>/<id2>/<id3>")
def delete_hyperspectral(id, id2, id3):
    des = mongo.db.hyperspectral.find_one(
        {"Datatype": id, "Camera_type": id2, "File_name": id3})

    if des ==None:
        return "There is no data"
    else:

        results = mongo.db.hyperspectral.delete_one(
            {"Datatype": id, "Camera_type": id2, "File_name": id3})

    
        return "delete data successful"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
